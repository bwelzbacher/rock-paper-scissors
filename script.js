$(document).ready(function() {
    $('#playAgain').hide();
    $('#RBS').hide();
    $('#SBP').hide();
    $('#PBR').hide();
    $('#PTP').hide();
    $('#RTR').hide();
    $('#STS').hide();

});

var rpsArray = ["Rock", "Paper", "Scissors"];
var selection = [];

function myArray (array, element) {
    for (var i=0; i<array.length; i++)
        if (array[i] == element) return true;
    return false;
}

function randomSelection (array) {
    var randomRPS = array [Math.floor(Math.random() * rpsArray.length)];
        if(!myArray(selection,randomRPS)) {
            selection.push(randomRPS);
            return randomRPS;
        }
        return randomSelection (array);
    }

for (var i=0; i<1; i++) {
    console.log(randomSelection(rpsArray));
}

function selectRock () {
    document.getElementById("choice").innerHTML = "Rock";
    }

function selectPaper () {
    document.getElementById("choice").innerHTML = "Paper";
    }

function selectScissors () {
    document.getElementById("choice").innerHTML = "Scissors";
    }

if (selection == "Scissors") {
    $(document).ready(function(){
        $('#rock').click(function () {
            document.getElementById("choice").innerHTML = "Rock breaks Scissors, You Win!";
            $('#RBS').show();
        })
        $('#paper').click(function () {
            document.getElementById("choice").innerHTML ="Scissors cut Paper, You Lose!";
            $('#SBP').show();
        })
        $('#scissors').click(function () {
            document.getElementById("choice").innerHTML = "It's a tie!";
            $('#STS').show();
        })
    });
}

if (selection == "Rock") {
    $(document).ready(function(){
        $('#rock').click(function () {
            document.getElementById("choice").innerHTML = "It's a tie!";
            $('#RTR').show();
        })
        $('#paper').click(function () {
            document.getElementById("choice").innerHTML = "Paper covers Rock, You Win!";
            $('#PBR').show();
        })
        $('#scissors').click(function () {
            document.getElementById("choice").innerHTML = "Rock breaks Scissors, You Lose!";
            $('#RBS').show();
        })
    });
}

if (selection == "Paper") {
    $(document).ready(function(){
        $('#rock').click(function () {
            document.getElementById("choice").innerHTML = "Paper covers Rock, You Lose!";
            $('#PBR').show();
        })
        $('#paper').click(function () {
            document.getElementById("choice").innerHTML = "It's a tie!";
            $('#PTP').show();
        })
        $('#scissors').click(function () {
            document.getElementById("choice").innerHTML = "Scissors cut Paper, You Win!";
            $('#SBP').show();
        })
    });
}

$(document).ready(function() {
    $('.buttons').click(function(){
        $('.buttons').hide();
        $('#playAgain').show();
    });
});

$(document).ready(function() {
    $('#playAgain').click(function() {
        location.reload();
    });
});